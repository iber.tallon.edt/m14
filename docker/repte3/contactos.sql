
CREATE DATABASE contactoss;

\c contactoss;

CREATE TABLE Contactos (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(255) NOT NULL,
    CorreoElectronico VARCHAR(255),
    Telefono VARCHAR(20)
);

INSERT INTO Contactos (Nombre, CorreoElectronico, Telefono)
VALUES
    ('Juan Pérez', 'juan@example.com', '123-456-7890'),
    ('María García', 'maria@example.com', '987-654-3210'),
    ('Carlos Rodríguez', 'carlos@example.com', '555-555-5555');


