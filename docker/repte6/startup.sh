#! /bin/bash

echo "COnfigurant el servei ldap al meu gust..."

sed -i "s/Manager/$DEBIAN_LDAP_ROOTDN/g" slapd.conf
sed -i "s/secret/$DEBIAN_LDAP_ROOTPW/g" slapd.conf

rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slap.d/*

slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
