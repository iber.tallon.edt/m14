## Pràctica de generar un servidor web funcionant en detach amb propagació de port basada en Debian latest en el curs 2023-2024

### Pasos fets:
**1. He creat un directori en la meva consola amb l'objectiu de posar-hi dins el fitxer Dockerfile, index.html i el script startup.sh.**
***
**2. He creat els fitxers Dockerfile, index.html i el script startup.sh:**
* index.html
***
```
                  <html>
                       <head>
	                    <title>ASIX-M14 WebServer</title>
                       </head>
                       <body>
                            <h1>Repte 2</h1>
                                 <p>Pràctica que consisteix en generar un servidor apache que funcioni en detach i que la propagació sigui del port del container al 
                                       port del host amfitrió.</p>
                       </body>
                  </html> 
```
***
* Dockerfile
***
                     FROM debian:latest
                                 -Això ens diu que el tipus de sistema operatiu amb el qual treballarem. En aquest cas, l'última versió de Debian.
                     LABEL author="iber@edt ASIX-M14"
                                 - Informació addicional
                     LABEL repte="Segon"
                                 - Informació addicional
                     RUN apt-get update && apt-get -y install procps apache2 vim tree iproute2 nmap
                                 - Primer actualitza els paquets i després instal·la els paquets que s'han especificat. 
                     RUN mkdir /opt/docker
                                 - Crea un directori. "No es necessari fer-ho".
                     COPY index.html /var/www/html/
                                 - Copia el meu fitxer HTML al directori del servidor web per defecte.
                     COPY startup.sh /tmp
                                 - Copia el script del directori actiu al directori /tmp.
                     RUN chmod +x /tmp/startup.sh
                                 - Li dona permisos d'execució al script.
                     CMD /tmp/startup.sh
                                 - Especifica que serà el programa per defecte una vegada posem en marxa el container.
                     EXPOSE 80
                                 - Informa al Docker que el container escoltarà el port 80 en posar-se en marxa. 
                     CMD apachectl -k start -X
                                 - Aquesta ordre es farà per defecte una vegada posem en marxa el container (després del CMD abans especificat).
***
* startup.sh
***
                     #! /bin/bash
                              - Indica al sistema operatiu que invoqui el "Shell" especificat per executar els comandaments que segueixin en el script.
                     service apache2 start
                              - Posa en marxa el servei web.
***
**3. He creat una imatge la qual tindrà tot el que hi ha en el meu directori actiu (Dockerfile, index.html i startup.sh):**
***
        docker build -t iberedt/repte2:detach .
***
**4. He entrat al container en background (segon pla) des d'una altra sessió:**
***
     docker run --rm -h web.edt.org -p 80:80 -d iberedt/repte2:detach
***
**5. Des d'una altra sessió, he entrat al container interactivament:**
***
        1.  docker ps
                - Per saber el nom del Docker que està en funcionament.
        2.  docker exec -it nom_container /bin/bash
                - Executa interactivament el container especificat.
***
**6. Dins del container he mirat si tenia el port obert i he buscat la meva IP:**
***
* nmap localhost
    - Mira els port que té oberts.
* ip a
    - Mira les IP.   
***
**7. He posat la IP al navegador i m'ha sortit el contingut del fitxer index.html.**
***
### Amb això el repte ha finalitzat.
