#! /bin/bash

if [ $# -eq 0 ]; then
  arg_buit="edtorg"
else
  arg_buit="$1"
fi

arg="$2"
case "$arg_buit" in
  "initdb")
    echo "Inicialitzant el servidor ldap amb les nostres dades fent populate de     edt-org"
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
  ;;
  "slapd")
    echo "Inicialitzant unicament el servidor ldap sense cap dades"
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
  ;;
  "start"|"edtorg") 
    echo "Inicialitzant el servidor ldap amb les dades predeterminades de ldap"
    /usr/sbin/slapd -d0
  ;;
  "slapcat")
    if [ -n "$arg" ]; then
      if [ "$arg" -eq "0" ] || [ "$arg" -eq "1" ]; then
  	slapcat -n "$arg"
      else 
	echo "S'ha de introduir com a segon argument un 0 o un 1"
      fi
    else
      slapcat
    fi

    ;;

  *)
   exit 1
   ;;
esac
