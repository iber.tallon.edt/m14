## Pràctica de generar un servidor ldap implementant una imatge configurable depenent del paràmetre rebut, funcionant en detach i amb persistència de dades basada en Debian latest en el curs 2023-2024

### Passos fets:
***
**1. He creat un directori en la meva consola amb l'objectiu de posar-hi dins el fitxer Dockerfile, edt-org.ldif, slapd.conf i el script startup.sh.**
***
**2. He creat els fitxers Dockefile, edt-org.ldif, slapd.conf i el script startup.sh:**
***
* Dockerfile
```
#! Servidor ldap

FROM debian:latest
LABEL autho="iber@edt ASIX-M14"
LABEL repte="cinquè"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT [ "/bin/bash",  "/opt/docker/startup.sh" ]
EXPOSE 389
```
***
* edt-org.ldif -> ([Contingut del fitxer](https://github.com/edtasixm06/ldap23/blob/main/ldap23%3Abase/edt-org.ldif "Contingut de edt-org.ldif"))
***
* slapd.conf -> ([Contingut del fitxer](https://github.com/edtasixm06/ldap23/blob/main/ldap23%3Abase/slapd.conf "Contingut de slapd.conf"))
***
* startup.sh
```
#! /bin/bash

arg=$2
case "$1" in
  "initdb")
	echo "Inicialitzant el servidor ldap amb les nostres dades fent populate de 	edt-org"
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
	slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
  ;;
  "slapd")
	echo "Inicialitzant unicament el servidor ldap sense cap dades"
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
	chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
  ;;
  "start"|"edtorg"|" ")
	echo "Inicialitzant el servidor ldap amb les dades predeterminades de ldap"
	/usr/sbin/slapd -d0
  ;;
  "slapcat")
	if [ -n "$arg" ]; then
  	if [ "$arg" -eq "0" ] || [ "$arg" -eq "1" ]; then
      slapcat -n "$arg"
  	else
    echo "S'ha de introduir com a segon argument un 0 o un 1"
  	fi
	else
  	slapcat
	fi

	;;

  *)
   exit 1
   ;;
esac
```
***
**3. He creat una imatge la qual tindrà tot el que hi ha en el meu directori actiu (Dockerfile, edt-org.ldif, slapd.conf i startup.sh):**
```
docker build -t repte5:base .
```
## Comprovació:
**He fet diferentes comprovacions tenint en compte el que he posat com argument en l'ordre per crear el container:**
***
### initdb -> Per poder entrar en el servidor ldap amb les meves dades.
**1. He engegat el container posant-li el nom repte5, amb propagació de ports, treballant en mode detach(background) i posant com a argument **initdb**.**
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -d repte5:base initdb
```
**2. Entro interactivament al container i comprovo si tot ha sortit bé:**
* Entrar al container:
```
docker exec -it repte5 /bin/bash
```
* Comprovació:
```
slapcat
```
* Resultat de la comprovació:
```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org
structuralObjectClass: organization
entryUUID: 23095090-005a-103e-88da-cddaf72cec92
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20231016102556Z
entryCSN: 20231016102556.592905Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20231016102556Z

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectClass: organizationalunit
structuralObjectClass: organizationalUnit
entryUUID: 2309f69e-005a-103e-88db-cddaf72cec92
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20231016102556Z
entryCSN: 20231016102556.597271Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20231016102556Z

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectClass: organizationalunit
structuralObjectClass: organizationalUnit
entryUUID: 230a96a8-005a-103e-88dc-cddaf72cec92
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20231016102556Z
entryCSN: 20231016102556.601368Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20231016102556Z

... 
```
* Surto del container i el paro
    * Sortir del container:
      ```
       exit
      ```
    * Parar la càmera
      ```
        docker stop repte5
       ```
***
### slapd -> Per poder entrar en el servidor ldap pero sense dades.
**1. He engegat el container posant-li el nom repte5, amb propagació de ports, treballant en mode detach(background) i posant com a argument **slapd**.**
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -d repte5:base slapd
```
**2. Entro interactivament al container i comprovo si tot ha sortit bé:**
* Entrar al container:
```
docker exec -it repte5 /bin/bash
```
* Comprovació:
```
slapcat
```
* Resultat de la comprovació: (No mostra res)
```
```
* Surto del container i el paro
    * Sortir del container:
      ```
       exit
      ```
    * Parar la càmera
      ```
        docker stop repte5
       ```
***
### start|edtorg|res (buit) -> Per entrar al servidor ldap amb les dades per defecte.
#### start
***
**1. He engegat el container posant-li el nom repte5, dos volums “un per la configuració (repte5-conf) i l'altre per a les dades (repte5-dades)”, amb propagació de ports, treballant en mode detach(background) i posant com a argument **start**.**
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -v repte5-dades:/var/lib/ldap -v repte5-conf:/etc/ldap/slapd.d -d repte5:base start
```
**2. Entro interactivament al container i comprovo si tot ha sortit bé:**
* Entrar al container:
```
docker exec -it repte5 /bin/bash
```
* Comprovació:
```
slapcat
```
* Resultat de la comprovació: 
```
dn: dc=nodomain
objectClass: top
objectClass: dcObject
objectClass: organization
o: nodomain
dc: nodomain
structuralObjectClass: organization
entryUUID: ee535864-0054-103e-841c-2d6a8f43d937
creatorsName: cn=admin,dc=nodomain
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.675271Z#000000#000#000000
modifiersName: cn=admin,dc=nodomain
modifyTimestamp: 20231016094840Z
```
* Surto del container i el paro
***
#### edtorg
***
**1. He engegat el container posant-li el nom repte5, dos volums “un per la configuració (repte5-conf) i l'altre per a les dades (repte5-dades)”, amb propagació de ports, treballant en mode detach(background) i posant com a argument **edtorg**.**
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -v repte5-dades:/var/lib/ldap -v repte5-conf:/etc/ldap/slapd.d -d repte5:base edtorg
```
**2. Entro interactivament al container i comprovo si tot ha sortit bé:**
* Entrar al container:
```
docker exec -it repte5 /bin/bash
```
* Comprovació:
```
slapcat
```
* Resultat de la comprovació: 
```
dn: dc=nodomain
objectClass: top
objectClass: dcObject
objectClass: organization
o: nodomain
dc: nodomain
structuralObjectClass: organization
entryUUID: ee535864-0054-103e-841c-2d6a8f43d937
creatorsName: cn=admin,dc=nodomain
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.675271Z#000000#000#000000
modifiersName: cn=admin,dc=nodomain
modifyTimestamp: 20231016094840Z
```
* Surto del container i el paro
***
#### res (buit)
***
**1. He engegat el container posant-li el nom repte5, dos volums “un per la configuració (repte5-conf) i l'altre per a les dades (repte5-dades)”, amb propagació de ports, treballant en mode detach(background) i no poso cap argument.**
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -v repte5-dades:/var/lib/ldap -v repte5-conf:/etc/ldap/slapd.d -d repte5:base 
```
**2. Entro interactivament al container i comprovo si tot ha sortit bé:**
* Entrar al container:
```
docker exec -it repte5 /bin/bash
```
* Comprovació:
```
slapcat
```
* Resultat de la comprovació: 
```
dn: dc=nodomain
objectClass: top
objectClass: dcObject
objectClass: organization
o: nodomain
dc: nodomain
structuralObjectClass: organization
entryUUID: ee535864-0054-103e-841c-2d6a8f43d937
creatorsName: cn=admin,dc=nodomain
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.675271Z#000000#000#000000
modifiersName: cn=admin,dc=nodomain
modifyTimestamp: 20231016094840Z
```
* Surto del container i el paro
***
### slapcat -> Depenent del segon argument que es posi, farà una mostra (slapcat) de la base de dades del servidor ldap per defecte “dimoni” o les dades de la meva base de dades.
#### nº1 
***
**1. He engegat el container posant-li el nom repte5, amb propagació de ports, treballant en mode interactiu (foreground) i posant com a primer argument “slapcat” i com a segon argument un “1”. **
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -it repte5:base slapcat 1
```
* Resultat:
```
dn: dc=nodomain
objectClass: top
objectClass: dcObject
objectClass: organization
o: nodomain
dc: nodomain
structuralObjectClass: organization
entryUUID: ee535864-0054-103e-841c-2d6a8f43d937
creatorsName: cn=admin,dc=nodomain
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.675271Z#000000#000#000000
modifiersName: cn=admin,dc=nodomain
modifyTimestamp: 20231016094840Z
```
***
####  res
**1. He engegat el container posant-li el nom repte5, amb propagació de ports, treballant en mode interactiu (foreground) i posant com a primer argument “slapcat” i cap a segon argument. **
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -it repte5:base slapcat 1
```
* Resultat:
```
dn: dc=nodomain
objectClass: top
objectClass: dcObject
objectClass: organization
o: nodomain
dc: nodomain
structuralObjectClass: organization
entryUUID: ee535864-0054-103e-841c-2d6a8f43d937
creatorsName: cn=admin,dc=nodomain
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.675271Z#000000#000#000000
modifiersName: cn=admin,dc=nodomain
modifyTimestamp: 20231016094840Z
```
***
####  nº0
**1. He engegat el container posant-li el nom repte5, amb propagació de ports, treballant en mode interactiu (foreground) i posant com a primer argument “slapcat” i com a segon argument un "0". **
```
docker run --rm -h ldap.edt.org --name repte5 -p 389:389 -it repte5:base slapcat 0
```
* Resultat:
```
cn=config
objectClass: olcGlobal
cn: config
olcArgsFile: /var/run/slapd/slapd.args
olcLogLevel: none
olcPidFile: /var/run/slapd/slapd.pid
olcToolThreads: 1
structuralObjectClass: olcGlobal
entryUUID: ee4e0c06-0054-103e-82ca-b5112409bf7b
creatorsName: cn=config
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.640457Z#000000#000#000000
modifiersName: cn=config
modifyTimestamp: 20231016094840Z

dn: cn=module{0},cn=config
objectClass: olcModuleList
cn: module{0}
olcModulePath: /usr/lib/ldap
olcModuleLoad: {0}back_mdb
structuralObjectClass: olcModuleList
entryUUID: ee4f13f8-0054-103e-82d2-b5112409bf7b
creatorsName: cn=admin,cn=config
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.647335Z#000000#000#000000
modifiersName: cn=admin,cn=config
modifyTimestamp: 20231016094840Z

dn: cn=schema,cn=config
objectClass: olcSchemaConfig
cn: schema
structuralObjectClass: olcSchemaConfig
entryUUID: ee4e3e56-0054-103e-82cd-b5112409bf7b
creatorsName: cn=admin,cn=config
createTimestamp: 20231016094840Z
entryCSN: 20231016094840.641865Z#000000#000#000000
modifiersName: cn=admin,cn=config
modifyTimestamp: 20231016094840Z

 ...
```
