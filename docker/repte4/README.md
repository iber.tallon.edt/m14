## Pràctica de generar un servidor ldap funcionant en detach amb persistència de dades basada en Debian latest en el curs 2023-2024

### Pasos fets:
***
**1. He creat un directori en la meva consola amb l'objectiu de posar-hi dins el fitxer Dockerfile, edt-org.ldif, slapd.conf i el script startup.sh.**
***
**2. He creat els fitxers Dockefile, edt-org.ldif, slapd.conf i el script startup.sh:**
***
* Dockerfile
```
# Servidor ldap
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
EXPOSE 389
```
***
* edt-org.ldif -> ([Contingut del fitxer](https://github.com/edtasixm06/ldap23/blob/main/ldap23%3Abase/edt-org.ldif "Contingut de edt-org.ldif"))
***
* slapd.conf -> ([Contingut del fitxer](https://github.com/edtasixm06/ldap23/blob/main/ldap23%3Abase/slapd.conf "Contingut de slapd.conf"))
***
* startup.sh
```
#! /bin/bash

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0
```
***
**3. He creat una nova xarxa anomenada 2hisx.**
```
docker network create 2hisx
```
***
**4. He creat una imatge la qual tindrà tot el que hi ha en el meu directori actiu (Dockerfile, edt-org.ldif, slapd.conf i startup.sh):**
```
docker build -t iberedt/repte4:base .
```
***
**5. He enxegat el container posant-li el nom "repte4", posant-li dos volums, un per la configuració i l'altre per a les dades (no és necessari crear els volums abans, automàticament es creen després de fer l'ordre.), posant-li la xarxa que anteriorment he creat i que treballi en mode detach(background).**
```
docker run --rm -h ldap.edt.org --name repte4 -v repte4-conf:/etc/ldap/slapd.d -v repte4-dades:/var/lib/ldap --net 2hisx -d iberedt/repte4:base
```
***
**6. Entro interactivament al container i comprovo si tot ha sortit bé:**
* Entrar al container:
```
docker exec -it repte4-dades /bin/bash
```
* Comprovació:
```
tree /etc/ldap/slapd.d/
```
- Resultat de la comprovació:
```
/etc/ldap/slapd.d/
|-- cn=config
|   |-- cn=module{0}.ldif
|   |-- cn=schema
|   |   |-- cn={0}corba.ldif
|   |   |-- cn={10}collective.ldif
|   |   |-- cn={1}core.ldif
|   |   |-- cn={2}cosine.ldif
|   |   |-- cn={3}duaconf.ldif
|   |   |-- cn={4}dyngroup.ldif
|   |   |-- cn={5}inetorgperson.ldif
|   |   |-- cn={6}java.ldif
|   |   |-- cn={7}misc.ldif
|   |   |-- cn={8}nis.ldif
|   |   `-- cn={9}openldap.ldif
|   |-- cn=schema.ldif
|   |-- olcDatabase={-1}frontend.ldif
|   |-- olcDatabase={0}config.ldif
|   |-- olcDatabase={1}mdb.ldif
|   `-- olcDatabase={2}monitor.ldif
`-- cn=config.ldif

3 directories, 18 files
```
***
