## Pràctica de crear una imatge personalitzada basada en Debian latest en el curs 2023-2024

### Passos fets:
**1. He creat un directori en la meva consola amb l'objectiu de posar-hi dins el fitxer Dockerfile.** 
***
**2. He creat el fitxer Dockerfile:**
***
     FROM debian:latest
             - Això ens diu que el tipus de sistema operatiu amb el qual treballarem. En aquest cas, l'última versió de Debian.
     LABEL author="@edt ASIX-M06"
             - Informació addicional  
     LABEL subject="imatge Debian personalitzada"
             -  Informació addicional
     RUN apt-get update && apt-get install -y procps iproute2 nmap tree vim
             -  Primer actualitza els paquets i després instal·la els paquets que s'han especificat. 
     RUN mkdir /opt/docker
             -  Crea un directori 
     COPY * /opt/docker/
             -  Copia tots el que tinguem en el directori actiu i el passarà al que hem creat
     WORKDIR /opt/docker
             -  Quan s'entri dins del docker, com a directori actiu tindrem "/opt/docker"
***
**3. He creat una imatge la qual tindrà tot el que hi ha en el meu directori actiu (Dockerfile):**
***
     docker build -t iberedt/repte1:latest .
***
**4. He entrat al container des d'un altre sessió:**
***
    docker run --rm -h ldap.edt.org -it iberedt/repte1:latest /bin/bash
***
**5. Per comprovar si tot ha sortit bé, he provat les ordres:**
* ps ax
* tree
* nmap 
* vim
* ip a

