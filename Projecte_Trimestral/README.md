# Projecte Trimestral - Primer Trimestre

### Objectiu de la pràctica
- Crear una pàgina web que utilitz-hi API-REST i autentificació OAUTH.
- Crear un grub de gitlab entre 2 o més persones on es troba el projecte final fet per tots els integrants.
- Presentar el treball de dues formes: Mitjançant una exposició i un poster.


### Pasos abans de la pràctica
- Me registrat ha [Barcelona Activa](https://www.barcelonactiva.cat/ "Registra Barcelona Activa") 
- Per poder seguir la pràctica m'he inscrit en el curs de <b>[Cibernàrium](https://cibernarium.barcelonactiva.cat/web/guest/ficha-actividad?activityId=952407"Curs")</b>.
- També tindré el [fitxer](https://gitlab.com/edtasixm14/m14-projectes/-/raw/master/docs/HowTo-ASIX-API-REST.pdf "HowTo ASIX API REST") donat pel profe per poder aprofondir amb els temes del projecte. 
<br><br>

## Curs

### Enllaços per ampliar i repassar conceptes
- API
    - https://ca.wikipedia.org/wiki/Interf%C3%ADcie_de_programaci%C3%B3_d%27aplicacions
- REST
    - https://ca.wikipedia.org/wiki/REST
- JSON
    - https://ca.wikipedia.org/wiki/JSON
    - http://www.json.org/
    - http://www.jsonlint.com/
- Servidor web
    - https://ca.wikipedia.org/wiki/Servidor_web
    - https://www.w3schools.com/jquery/jquery_callback.asp
- AJAX
    - https://ca.wikipedia.org/wiki/XMLHttpRequest
    - https://www.w3schools.com/js/js_ajax_http.asp
    - https://medium.freecodecamp.org/here-is-the-most-popular-ways-to-make-an-http-request-in-javascript-954ce8c95aaa
    - https://www.w3schools.com/jquery/jquery_ref_ajax.asp
    - https://www.w3schools.com/jquery/jquery_ajax_get_post.asp
    - https://www.w3schools.com/js/js_ajax_http_response.asp
    - https://www.w3schools.com/tags/ref_httpmessages.asp
- CSS
    - https://css-tricks.com/snippets/css/a-guide-to-flexbox/
- JQUERY
    - https://www.w3schools.com/jquery/jquery_ref_html.asp
    - https://www.w3schools.com/jquery/jquery_callback.asp


### Lliçons

<b>1. Què és una API en REST?</b>
- <ins>Interaccions d'una API en REST</ins>
    - Al any 2000 gracies a la tesi de Roy Thomas Fielding, s'inposa una nova arquitectura per aprofitar el protocol HTTP en el ambit de desenvolupament d'aplicacions distribuïdes a través de la xarxa. <br>
Aquesta arquitectura s'anomena REST o RESTful la qual va sobstituir a arquitectures com SOAP o XML-RCP.

    - Un servidor web REST a de satisfer sis restirccions:
        - Rendiment
        - Escalabilitat
        - Simplicitat
        - Modificabilitat
        - Portabilitat
        - Fiabilitat

    - Conceptes claus:
        - Uniform Interface
            - Part fundamental del servei REST.
            - Defineix la interficie entre client-servidor
            - Principis:
                - Identificació dels recursos
                    - Recursos individuals identificacts en les peticions mitjançant els URI o URL.
                - Manipulació dels recursos
                    - Un client mentre tingui permisos i mitjançant la representació d'un recurs,te informació per modificar i esborrar el recurs del servidor.
                - Missatges autodescriptius
                    - Cada missatge intercanviat entre el client i el servidor conté la informació necessària per processar-lo.
                - HATEOAS (Hypermedia As The Engine Of Application State)
                    - 

        - Client-Server
            - La comunicació entre client i servidor no requereix que el servidor hagi de guardar informació del client entre peticions consecutives.
        - Stateless
        - Cacheable
        - Layered system
        - Code on Demand(opcional)
        - AJAX (Asynchronous JavaScript And XML)
            - Les aplicacions web dinàmiques l'otilitzen per fer les peticions i procesa les respostes provinents del servidor REST.
            - Permet:
                - Llegir dades d’un servidor web (una vegada la pàgina s’ha carregat).
                - Actualitzar una pàgina web sense haver de recarregar la pàgina.
                - Enviar dades a un servidor web.
            - Fa transport de dades tant en XML com via JSON. D'aquesta manera pots fer aplicacions webs interactibes que treballin amb multitud de recursos provinents de API's externes i treballar amb la resposta d’una manera molt lleugera.
            - Les dades que són demanades es carreguen en segon pla.

- <ins>Com es vincula REST amb HTTP</ins>
    - Metodes HTTP per poder fer peticions a un servidor:
        - GET
            - Per demanar dades d'un recurs específic. Per exemple: URI, ENDPOINT(API REST)
            - No pots modificar les dades del servidor.
            - Es poden interceptar per l'URL.
        - POST
            - Recull dades d'un servidor i alhora deixa en la mateixa petició crear o actualitzar un recurs del servidor.
            - Les dades queden definides en la petició AJAX.
            - No es poden interceptar per l'URL, per tant, és més segur que GET.
        - PUT
            - Envia dades a un servidor per tal de crear o actualitzar un recurs.
            - Permet repetir una mateixa petició POST a diferents parts del nostre codi.
        - DELETE
            - Esborra un recurs determinat.
    - Exemple d'utilització dels metodes HTTP:
        - GET
            ```
            curl -X GET https://jsonplaceholder.typicode.com/posts
            ```
    - Metodologia AJAX
        - XMLHttpRequest
            - Element més important d'AJAX fet en JavaScript.
            - Actualitzar una pàgina web sense haver de recarregar la pàgina sencera.
            - Una vegada la pàgina s'ha carregat:
                - Fer una petició de dades a un servidor. 
                - Rebre dades d'un servidor.
            - Enviar dades al servidor (background).
        - Funcionament 'client-servidor'
            1. Un usuari o usuària realitza una recerca a Google.
            2. EL client (Google) envia una petició "HTTP request" a la web.
            3. El servidor web rep la petició.
            4. El servidor executa l'aplicació que processa la petició. 
            5. El servidor retorna una resposta "HTTP responese" (output) al client.
            6. EL client rep la resposta.
        - Prova pràtica de AJAX
            1. He obert una nova pestanya en chrome.
            2. Desde inspect, vaig a la consola i poso aquesta ordre:
            ```
            new XMLHttpRequest();
            ```
            3. Mirar el resultat posant la flexeta cap a vaig:
            ```
            XMLHttpRequest { onreadystatechange: null, readyState: 0, timeout: 0, withCredentials: false, upload: XMLHttpRequestUpload, responseURL: "", status: 0, statusText: "", responseType: "", response: "" }
            ​
            mozAnon: false
            ​
            mozSystem: false
            ​
            onabort: null
            ​
            onerror: null
            ​
            onload: null
            ​
            onloadend: null
            ​
            onloadstart: null
            ​
            onprogress: null
            ​
            onreadystatechange: null
            ​
            ontimeout: null
            ​
            readyState: 0
            ​
            response: ""
            ​
            responseText: ""
            ​
            responseType: ""
            ​
            responseURL: ""
            ​
            responseXML: null
            ​
            status: 0
            ​
            statusText: ""
            ​
            timeout: 0
            ```
- <ins>Video tutorial I: Exercicis amb AJAX i jQuery</ins>
    
    - Lloc on he fet l'[exercici](https://gitlab.com/iber.tallon.edt/m14/-/blob/main/Progecte%20Trimestral/Tutorials/cas1.html)

    - Pasos fets:
        1. Buscar per google "api rest online"

        2.  Entrem [aqui](https://jsonplaceholder.typicode.com/)

        3. On diu <b>Resources</b> li donem a "[/posts](https://jsonplaceholder.typicode.com/posts)".

        4. Creo un fitxer HTML on programaré .

        5. Obro el fitxer HTML desde google.
            - Faig clic dret i vaig a 'inspecte'.
            - A continuació vaig a la consola. Observo que hi posa "Array(100)", clico i veig l'Array de JS retornat pel servidor després de fer la petició AJAX.
            - Aquest array conté els 100 objectes JS que estaben al [JSON](https://jsonplaceholder.typicode.com/posts) on he recoolit les dades.

- <ins>Prova1</ins>
    - Requisits:
        - [Web](https://jsonplaceholder.typicode.com/) a utilitzar. 
            - Emprar l’endpoint de la seva API ‘/posts/1/comments’ fent peticions AJAX amb el mètode GET. 
            - Mostrar el resultat per la consola.
        - En el HTML mostrar les propietats "id" i "email" de cadascun dels cinc elements del JSON, separats entre ells per un salt de línia.
        - Mostrar les mateixes propietats que abans però únicament del primer element de l’array. Serà suficient mostrar el valor en consola.

    - [Fitxer](https://gitlab.com/iber.tallon.edt/m14/-/blob/main/Progecte%20Trimestral/Proves/prova1.html)
        
<b>2. Sol·licituds de l'API</b>

- IQuery
    - Conceptes clau:
        - Framework o llibreria de JS. 
        - Facilita la manipulació dels objectes de l'HTML.
        - La seva API és acceptada per tots els navegadors
    - Com instal·lar-lo:
        - Vaig a la <ins>[pàgina web](https://jquery.com/)</ins>
        - Clico en [Download](https://jquery.com/download/) i segueixo el metode d'instal·lació. En el meu cas, he fet l'instal·lació amb npm.

- JSON (JavaScript Object Notation)
    - Conceptes clau:
        - Format lleuger d'emmagatzematge i intercanvi de dades.
        - Fàcil d'entrendre, ja que només és text amb sintaxi JS.
        - Quan s’intercanvien dades entre un servidor i un client, les dades només poden ser de text.
        - JSON és una manera de treballar amb les dades directament com a objectes de JS.
        - Permet guardar objectes de JS com a arxius bàsics de text i poder comparti-los entre servidor. 
            - És f'a amb el mètode JSON.stringfy().



- <ins>Recursos de les API</ins>

    - Amb el JQuery instal·lat, ja el podré utilitzar a la meva aplicació web fent referencia al arxiu amb el que vulgui treballar en un script especeific al meu HTML. 
        - Arxiu que utilitzaré per defecte:
            - jquery.min.js
    - Mètode  $.getJSON()
        - Permet fer peticions AJAX sempre que estiguem recollint un JSON de l’endpoint on fem la consulta.
    - Mètode  $.get()
        - És equivalent a fer una petició XMLHttpRequest() utilitzant el mètode HTTP GET en JavaScript.
    - Mètode  $.ajax()
        - Permet fer una consulta i manipular també el resultat de l’error a diferència del dos mètodes anteriors.
    - Mètode $.each 
        -  Permet recórrer l’array d’objectes JavaScript que obtinguem com a resposta, adjudicant a cada objecte una clau ‘key’ en forma de comptador ascendent i un ‘value’ que en aquest cas serà cadascun dels objectes de l’array que li passem com a paràmetre al bucle. 
    - $(document).ready()
        - mètode jQuery que fa que un cop el ‘document’ hagi enllestit tota la resta de processos, executi la funció de callback que li passem entre claus, com podria ser una petició AJAX.
    - mètode jQuery ‘click’ 
        - Permet vincular l’execució de la petició AJAX a qualsevol element de la nostra vista.
    - mètode jQuery ‘append’ 
        - permet penjar els valors de les propietats de l’objecte a qualsevol element de la nostra vista o de nous elements que podem crear directament en jQuery dins de la funció de callback del bucle $.each().
    - JSON.stringify()
        - Permet convertir un objecte de JS a JSON per poder enviar-lo al servidor.
    - JSON.parse()
        - Permet convertir un JSON guardat al servidor en un objecte de JS.

<b>3. Resposta de l'API</b>
- <ins>Missatges de Status HTTP</ins>
    - Missatges de resposta i error més comuns de les API.
        - Propietats principals que conté l'objectes que ens retorna el servidor cada cop que fem una nova petició:
            - readyState 
                - Conté l’estatus de la crida.
            - onreadystatechange 
                - Defineix la funció que ha de ser executada quan readyState canvia.
            - status/statusText
                - Contenen els missatges d’estatus HTTP de l’objecte XMLHttpRequest.
        - Valors principals que pot prendre la propietat readyState:
            - 0
                - Petició no inicialitzada.
            - 1
                - Connexió de servidor establerta.
            - 2
                - Petició rebuda.
            - 3
                - Petició en procés.
            - 4 
                - Petició finalitzada i resposta llesta.
        - Valors més comuns que pren la propietat status en el cas que la petició AJAX es processi satisfactòriament:
            - Status:
                - Status 200 = procés excitós.
                - Status 40[0-4] = errors de la part client.
                - Status 500 i 503 = errors de servidor.
            - Status 200 
                - Resposta estàndard quan la petició està OK.
            - Status 201 
                - En cas de peticions per POST, petició completada i nou recurs creat.
            - Status 400 (Bad Request )
                - La petició no es pot completar per un error de sintaxi.
            - Status 401 (Unauthorized)
                - La petició és legal però el servidor la rebutja. 
                - Acostuma a saltar en els casos que l’autorització és necessària però errònia.
            - Status 403 (Forbidden)
                - La petició és legal però el servidor la rebutja.
            - Status 404 (Not found)
                - L’URL indicat no es troba.
            - Status 500 (Internal Server Error)
                - Missatge d’error genèric.
            - Status 503 (Service Unavailable)
                - El servidor no està disponible.
        - Exemples:
            - Crida 1
                - readyState = 4
                - status = 200
                - statusText = ""
                ```
                var xmlhttp = new XMLHttpRequest();

                var url = "https://api.github.com";

                    xmlhttp.onreadystatechange =   function() {

                        if (this.readyState == 4 && this.status == 200) {

                            var myArr = JSON.parse(this.responseText);

                            console.log(myArr);:

                        }
        
                    };

                    xmlhttp.open("GET", url, true);

                    xmlhttp.send();

                    console.log(xmlhttp);
                ```
            - Crida 2
                - readyState = 4
                - status = 401
                - statusText = Unauthorized
                ```
                    var xmlhttp = new XMLHttpRequest();

                    var url = "https://api.github.com/user";

                    xmlhttp.onreadystatechange = function() {

                    if (this.readyState == 4 && this.status == 200) {

                        var myArr = JSON.parse(this.responseText);

                        console.log(myArr);

                        }

                    };

                    xmlhttp.open("GET", url, true);

                    xmlhttp.send();

                    console.log(xmlhttp);
                ```

    - Tutorial
        - Crear-se un compte
        - Anar a la [web](https://developer.spotify.com/) de developers de Spotify.
            - Anar a [dashboard](https://developer.spotify.com/dashboard)
            - Crear app
        - Anar a [Node.js](https://nodejs.org/en) i instal·lar LTS.
        - Descarregar carpeta [projecte Spotify](https://github.com/spotify/web-api-examples)
            - Descomprimir-la
            - Anar a la ruta "(arciu)/authorization\authorization_code
                - Observar dos coses:
                    - public (directory)
                        - Directory on allutjar totes les pàgines webs públiques de la meva aplicació. 
                    - app.js
                        - Modificar les variables:
                            - client_id
                                - El ID client que trobo en la informació basica de la meva app de Spotify.
                            - client_secret
                                - La clau secreta que trobo en la informació basica de la meva app de Spotify.
                            - redirect_uri
                                - La URL on rediccionaré la meva pàgina web de Spotify. Per defecte és el port [8888](http://localhost:8888/callback).
                                    - Si he de canviar el port, he de modificar les ultimes dues línies:
                                        -  console.log('Listening on 8888');
                                        - app.listen(8888);
            - Fer ordres:
                - npm install
                - npm run
                - npm start
            - Buscar pel navegador: http://localhost:8888


<b>4. Relació entre sol·licitud i resposta</b>




