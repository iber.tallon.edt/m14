# <center>Repte 2</center>
## <center><u>Provisioning de README, git i docker</u></center>

<br />

#### Fitxers utilitzats:

- <i>Vagrantfile</i>
- <i>start.sh</i>
- <i>useradd.sh</i>

<br />

#### Passos fets:

1. 
    ```
    vagrant init generic/alpine38
    ```

2. 
    ```
    vim Vagrantfile
    ```

3. 
    ```
    vim start.sh
    ```

4. 
    ```
    vagrant up
    ```

<br />

#### Comprovacions:

1. Mirar si s'han creat els usuaris.
    ```
    cat /etc/passwd

    vagrant:x:1000:1000:Linux User,,,:/home/vagrant:/bin/bash
    user1:x:1001:1001:Linux User,,,:/home/user1:/bin/bash
    user2:x:1002:1002:Linux User,,,:/home/user2:/bin/bash
    user3:x:1003:1003:Linux User,,,:/home/user3:/bin/bash

    # ---------------------------------------------------

    ls -l /home

    total 16
    drwxr-sr-x    2 user1    user1         4096 Apr  5 10:54 user1
    drwxr-sr-x    2 user2    user2         4096 Apr  5 10:54 user2
    drwxr-sr-x    2 user3    user3         4096 Apr  5 10:54 user3
    drwxr-sr-x    3 vagrant  vagrant       4096 Jan  9 20:00 vagrant
    ```

2. Comprovar que s'ha instal·lat correctament tant git com docker. 
- git

    ```
    apk search docker

    docker-bash-completion-18.06.1-r0
    docker-18.06.1-r0
    openvswitch-2.9.2-r0
    ...
    ```

- docker
    ```
    apk search git

    git-bash-completion-2.18.4-r0
    py-pygit2-0.27.1-r0
    git-cvs-2.18.4-r0
    ...
    ```

3. Miro si els usuaris creats tenen el grup docker.

    ```
    su - user1
    Password:

    id
    uid=1001(user1) gid=1001(user1) groups=102(docker),1001(user1)

    docker ps
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

    ```

4. Paro la màquina virtual i la destrueixo.

    ```
    vagrant halt
    ==> default: Attempting graceful shutdown of VM...

    vagrant destroy
    default: Are you sure you want to destroy the 'default' VM? [y/N] y
    ==> default: Destroying VM and associated drives...
    ```