#! /bin/bash
# 
# iber@edt ASIX-M14 
# Vagrant-Repte2
#
# Script que crea tres usuaris 
# -----------------------

for user in user1 user2 user3
do
    adduser -D -h /home/$user $user
    echo -e "$user:$user" | chpasswd $user
done