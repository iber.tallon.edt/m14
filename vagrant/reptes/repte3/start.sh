#! /bin/bash
# 
# iber@edt ASIX-M14 
# Vagrant-Repte3
#
# Script que instal·la l'eina "git" i es baixa el repositori M06-ASO(https://gitlab.com/edtasixm06/m06-aso.git) i M14-Projecte(https://gitlab.com/edtasixm14/m14-projectes).
#
# -----------------------

# Instal·la l'eina git
apk add git 

# Crea un directori on dins es posaran dos repositoris de git.
mkdir -p /usr/share/doc/edtasix
cd /usr/share/doc/edtasix
git clone https://gitlab.com/edtasixm06/m06-aso.git
git clone https://gitlab.com/edtasixm14/m14-projectes