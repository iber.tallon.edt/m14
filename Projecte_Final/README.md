Iber Tallón

# <center>Projecte Trimestral - Tercer trimestre - Projecte Final</center>

![Alt text](img/image1.png)
<i>[Nota1*](#nota1)</i>

<br />

#### Requisits generals

- Projecte individual.

- Aquest projecte es desenvolupa en un projecte gitlab amb una adreça que cal indicar als professors un cop es comenci el projecte.

- Cal fer tota la documentació en markdown.

- La exposició del projecte a de tindre un temps màxim de 20 minuts, i una demostració pràctica també de 20 minuts com a màxim.

- Part de l’exposició a de ser en anglès.

- Cal elaborar un póster i un vídeo de presentació (seriós) d’almenys un minut i mig i no major de tres minuts.

- La documentació ha d’estar tota sencera en català o anglès. **S’ha de repassar l’ortografia!!!**

<br />

#### Objectiu del projecto

- Implementació d'un SAMBA AD
    - Instal·lació de SAMBA AD
    - Resolució DNS
    - Gestió d'usuaris
    - Integració de màquines al domini
    - Integració de sistemes GNU/LINUX i Windows
<br />


## Index

- **[Introducció](#Introducció)**
    - [Creació del entornde treball](#work-environment)

- **[Implementació d'un SAMBA AD](#fase1)**
    - [Instal·lació SAMBA 4 AD](#install-AD)
    - [Configuració bàsica](#config-bas)
    - [Xarxa](#xarxa-AD)
    - [Configuració SAMBA 4 AD](#config-AD)

- [Creació màquines virtuals](#create-VM)
    - **[Windows 11](#windows)**
       - [Instal·lació Windows](#install-win)
       - [Configuració Windows](#config-win)
    - **[Debian 12](#debian)**
       - [Instal·lació Debian](#install-deb)
       - [Configuració Debian](#config-deb)

<br /><br />

## Introducció<a name="Introducció"></a>


### Creació del entornde treball<a name="work-environment"></a>

# **Instal·lació i configuració SAMBA Server**<a name="fase1"></a>

## **Instal·lació**<a name="install-AD"></a>

Creació i instal·lació màquina virtual

![Creació i instal·lació màquina virtual](img/create.png){width="5.2453in" height="3.0756in"}

Assignar-li en la configuració de la xarxa que sigui un adaptador pont (bridged Adapter) amb la xarxa on es troba el meu ordinador. Això ho faig per poder treballar remotament:

![](img/samba_bridge.png){width="4.7244in" height="3.398in"}

Per connectar-se a internet \"10.0.2.15\" (ip per defecte de VirtualBox)

![](img/samba_nat.png){width="6.2681in" height="4.5693in"}

Crear xarxa per al domini de SAMBA \"192.168.1.0/24\"

![](img/samba_xarxa.png){width="5.4839in" height="3.972in"}

Creació usuari i host samba server (passwd:server)

![](img/samba_server.png){width="4.3484in" height="2.2425in"}

-   passwd: server

## **Configuració bàsica**<a name="config-bas"></a>

Entro com a superusuari

![](img/sudo_su.png){width="2.0417in" height="0.198in"}

Miro el hostname que tinc en la màquina virtual

![](img/hostname_server.png){width="2.802in" height="0.3543in"}

Configuro el hostname amb el nom adequat al projecte

![](img/hostname_ctl.png){width="4.4791in" height="0.1665in"}

Actualitzo els paquets i repositoris de la màquina virtual i instal·lo
els paquets necessaris per a la configuració del Samba AD

![](img/aptget_update.png){width="3.3126in" height="0.198in"}

![](img/install_paquetes.png){width="6.8654in" height="0.4453in"}

Mentre es fa la instal·lació, es mostraran diversos missatges de
Kerberos, els hauré d'omplir de la següent manera:

![](img/kerberos.png){width="5.5228in" height="1.8035in"}

![](img/kerberos_servidors.png){width="6.2681in" height="1.139in"}

![](img/kerberos_servidor_adm.png){width="6.2681in" height="1.0835in"}

## **Xarxa**<a name="xarxa-AD"></a>

Modifico el fitxer de configuració de netplan per poder posar una IP
estàtica en la interfície on habia posat anteriorment la xarxa interna
(192.168.1.0/24)

![](img/edit_netplan.png){width="5.6354in" height="0.198in"}

![](img/cfg_netplan.png){width="4.3228in" height="2.052in"}

Provo si la configuració està ben feta i aplico els canvis

![](img/projecte_html_340100a7b2b993fa.png){width="3.1146in" height="0.2189in"}

![](img/projecte_html_4122b36ecb9ab694.png){width="3.2811in" height="0.2189in"}

Com puc veure s'ha establert correctament la IP estàtica

![](img/projecte_html_42ada3474546d781.png){width="6.2681in" height="2.861in"}

Modifico el fitxer de configració hosts amb les següents línies

![](img/projecte_html_1deef92034b3482a.png){width="3.3335in" height="0.2398in"}

![](img/projecte_html_fedba109a6910f93.png){width="6.2681in" height="2.3752in"}

Comprovo fent ping a dc i dc.iber.org

![](img/projecte_html_9231f61f3afb86d0.png){width="5.3543in" height="2.7189in"}

Desactivo el servei systemd-resolved, ja que no té compatibilitat amb
l'eina samba i amb aquest servei encès, podrien haver problemes

![](img/projecte_html_eb504a6a475339d2.png){width="6.2681in" height="0.5138in"}

Trec el link que té el fitxer de configuració de resolució de noms per
poder crear el meu propi fitxer

![](img/projecte_html_3e05b8329d7e86dc.png){width="4.1043in" height="0.2291in"}

![](img/projecte_html_f61af69805ee5b53.png){width="1.9272in" height="0.5835in"}

-   Això ho faig perquè la mateixa màquina virtual pugui resoldre el
    domini que vull crear per al meu servidor Samba AD

Desactivo i per conseqüència paro els serveis smbd, nmbd i winbind, ja
que no són necessaris per crear el servidor Samba AD

![](img/projecte_html_b93be77e126d7a77.png){width="6.2681in" height="1.4165in"}

## **Configuració Samba4 AD**<a name="config-AD"></a>

Creo una còpia de seguretat del fitxer de configuració de samba

![](img/projecte_html_138f3c9a25a9af8c.png){width="6.1354in" height="0.2291in"}

Faig la següent ordre per crear el meu domini Active Directory en el meu
servidor samba

![](img/projecte_html_19a7cea497b14e65.png){width="6.2681in" height="1.1252in"}

-   Com es pot veure en la imatge, havent fet les configuracions
    anteriorment, ja venen per defecte les opcions.

Faig una còpia de seguretat del fitxer de configuració de Kerberos

![](img/projecte_html_a8038ed2fa94b1c2.png){width="5.2811in" height="0.2291in"}

Em creo una còpia del fitxer /var/lib/samba/private/krb5.conf, el qual
tindrà la configuració predefinida per funcionar amb el meu nou domini
(iber.org)

![](img/projecte_html_13367e51dccc2bf9.png){width="6.2681in" height="0.25in"}

Desemmascaro el servei de Samba Active Directory Domain Controller

![](img/projecte_html_c4b8f54d8235c68.png){width="4.5in" height="0.3437in"}

Enxego el servei de Samba Active Directory Domain Controller

![](img/projecte_html_9fde152c0541c9c3.png){width="4.4583in" height="0.2189in"}

Miro l'estat del servidor

![](img/projecte_html_b37061ff236dd3bb.png){width="4.4689in" height="0.2083in"}

## **Comprovacions de la configuració Samba4 AD**

Verifico si els noms de domini estan ben configurats

![](img/projecte_html_c277ba8dc1f9aedc.png){width="3.0728in" height="0.8925in"}

Verifico que tant ldap com Kerberos apunten correctament al servidor
Samba AD

![](img/projecte_html_216befa59f34ce92.png){width="4.2083in" height="0.5811in"}

Un cop comprovat que Kerberos està apuntan correctament al servidor
Samba, m'autentico a Kerberos

![](img/projecte_html_45e0e4eee8990bdc.png){width="6.1874in" height="0.552in"}

Miro els tiquets de Kerberos obtinguts

![](img/projecte_html_1fe1136bf8281e05.png){width="5.2602in" height="1.2189in"}

Comprovo si puc accedir als recursos predeterminats del meu domini

![](img/projecte_html_64e3286f5c65483f.png){width="4.8154in" height="2.5681in"}

Em connecto al recurs compartit **netlogon **com a usuari administrador

![](img/projecte_html_d515bb7ced76ca8c.png){width="5.4799in" height="1.3075in"}

Verifico que la configuració de Samba no estigui incorrecte

![](img/projecte_html_ed4a8c6cd7306ad0.png){width="2.5311in" height="0.25in"}

![](img/projecte_html_20a837dc80776f8a.png){width="3.1228in" height="3.9898in"}

Verifico els nivells funcionals actuals del meu entorn Active Directori
gestionat pel servidor Samba

![](img/projecte_html_48ff84ca578d0154.png){width="4.3457in" height="0.8866in"}

Creo un nou usuari samba anomenat **kilian **i l'habilito

![](img/projecte_html_e014c5a8bb1c1877.png){width="4.3228in" height="0.7083in"}

-   Passwd: 'Kilian01'

![](img/projecte_html_a9ea27c21dbd963e.png){width="4.5626in" height="0.3646in"}

Creo un nou usuari samba anomenat **ton** i l'habilito

![](img/projecte_html_729d98de9210c707.png){width="4.0102in" height="0.6665in"}

-   Passwd: 'Tonclie01'

![](img/projecte_html_7eb353e15a681605.png){width="4.3126in" height="0.3646in"}

Verifico que s'han creat correctament els nous usuaris kilian i ton

![](img/projecte_html_258e9c8a999ba9df.png){width="3.0398in" height="0.8874in"}

# **Instal·lació i configuració client Windows**<a name="windows"></a>

## **Instal·lació**<a name="install-win"></a>

Creació i instal·lació màquina virtual

![](img/projecte_html_fde4f0aec7cd969a.png){width="4.9535in" height="2.2756in"}

Crear xarxa per al domini de SAMBA \"192.168.1.0/24\"

![](img/projecte_html_5e0e9426856459e.png){width="4.3264in" height="3.1083in"}

Assignar-li en la configuració de la xarxa que sigui un adaptador pont
(bridged Adapter) amb la xarxa on es troba el meu ordinador. Això ho
faig per poder accedir a internet .

![](img/projecte_html_9d4e6fe893baf79e.png){width="4.6098in" height="2.7429in"}

**Passos importants d'instal·lació de Windows 10:**

-   S'ha de tenir en compte que com a Sistema Operatiu he escolit
    Windows 10 Pro, ja que es el sistema de Windows que permet tenir
    Active Directory.
-   En el tipus d'instal·lació, s'ha d'escollir com a
    **Personalitzada**, ja que es l'unica opció que et deixa fer.
-   Quan demana de connectar-se a una xarxa, s'ha de clicar en "**no
    tinc internet**".
-   A partir de fer la creació d'usuari, he anat indicant que no a totes
    les ofertes que Windows m'ha danat.

Creació d'usuari

![](img/projecte_html_a1e25c7ec19c6815.png){width="2.422in" height="1.6402in"}

![](img/projecte_html_44019e2e51e80f5d.png){width="2.4028in" height="1.2728in"}

-   *passwd:clientsamba*

## **Xarxa** <a name="config-win"></a>

Entro dins del cmd de Windows i poso la següent ordre

![](img/projecte_html_7cbec6efaa863c47.png){width="4.6772in" height="2.6819in"}

-   *Aquesta ip que mostra ens diu que no tenim cap paràmetre de
    configuració de xarxa.*

Faig clic derecho per poder entrar en propietats del cmd

![](img/projecte_html_c0cf9750f866a78d.png){width="2.6402in" height="1.3634in"}

Vaig a "Fuente" i poso que tingui un tamany de 24

![](img/projecte_html_b0ed0f39cf3f608a.png){width="2.7965in" height="2.3717in"}

Vaig a l'apartat de xarxa

![](img/projecte_html_4fb26edb8e515998.png){width="2.1874in" height="1.1602in"}
                 
Entro dins de opcions d'adaptador

![](img/projecte_html_4245a6d5f1f317de.png){width="2.5311in" height="2.2929in"}

Clico en propietats de l\'adaptador

![](img/projecte_html_e163ef3dff349d00.png){width="3.7673in" height="1.6126in"}

![](img/projecte_html_a39ab3a869574684.png){width="2.2673in" height="2.6319in"}

![](img/projecte_html_48acf41877194c78.png){width="2.6563in" height="3.3071in"}

Assigno la IP estàtica que tindrà el Windows dins de la xarxa interna i
li poso la seva màscara de xarxa

![](img/projecte_html_3b24e6f8fd0fb61d.png){width="2.9953in" height="3.1661in"}

Un cop assignada la IP, torno al cmd

## **Connectar-se al domini**

Vaig al menú d'ajustaments del meu usuari

![](img/projecte_html_60b71cec8e8a01e2.png){width="3.172in" height="2.9402in"}

Vaig a vaig de tot em **Acerca de**

![](img/projecte_html_baf800cd4f2e36c6.png){width="1.8744in" height="3.0091in"}

Vaig a l'opció avançada de **canviar el nom de l'equip**

![](img/projecte_html_fe3fd1c6b061dd5c.png){width="2.4146in" height="1.5835in"}

Vaig a l'opció de Canviar

![](img/projecte_html_2c9a05d3f8b41469.png){width="2.728in" height="1.9874in"}

En entrar em surt aquesta configuració per defecte.

![](img/projecte_html_d5094e661640a840.png){width="2.1098in" height="2.5181in"}

Indico que vull ser membre del meu domini i accepto els canvis fets.

![](img/projecte_html_48472b2e1ee1341e.png){width="2.161in" height="2.6646in"}

Poso l'usuari administrador del domini amb la seva contrasenya

![](img/projecte_html_3d564dd81b3bd2c1.png){width="3.161in" height="2.2063in"}

Un cop posada la contrasenya em surt un missatge confirmant que m'he
unit correctament al meu domini de Samba

![](img/projecte_html_f7d1b023e9b1f824.png){width="2.5465in" height="1.1472in"}

![](img/projecte_html_b2b94867e2a46549.png){width="2.3244in" height="1.1756in"}

Reinicio la màquina de Windows perquè es guardin els canvis de
configuració que he fet

![](img/projecte_html_63122c817fdb9f0c.png){width="1.5728in" height="1.2917in"}                         

Esborro el disc d'instal·lació perquè quan s'engegui la màquina no es
posi en mode d'instal·lació

![](img/projecte_html_fe1b444260f99b01.png){width="4.7811in" height="3.3929in"}

## **Comprovacions**

Primer de tot miro des del meu usuari local si tla configuració de la
xarxa la tinc correctament configurada.

Confirmo que la IP s'ha creat correctament

![](img/projecte_html_672f04a7e4e690d4.png){width="3.5866in" height="1.3244in"}

Comprovo si tinc connectivitat amb el servidor Samba

![](img/projecte_html_4e487146ff17cdf6.png){width="3.0161in" height="1.4681in"}

Ara que ja he confirmat que hi ha connectivitat, provo d\'entrar ha una
sessió com a un usuari de samba.

Entro en la sessió amb el meu usuari samba **Kilian** dins del meu
domini **iber.org**

![](img/projecte_html_65c1a606364f1bc5.png){width="2.522in" height="3.0366in"}

Un cop dins entro en l'arxivador del sistema i me\'n vaig a l'apartat de
xarxa.

Habilito que es puguin veure entre xarxes

![](img/projecte_html_2177e3c7b70a728b.png){width="6.2681in" height="1.5835in"}

Activo la detecció de xarxes

![](img/projecte_html_98b141bc9040fd4d.png){width="6.2681in" height="0.5972in"}

Permeto fer canvis des de l'usuari Kilian

![](img/projecte_html_b943a3e0f2960095.png){width="2.5256in" height="2.3154in"}

Des del navegador de xarxa, miro si puc accedir als recursos del meu
domini

![](img/projecte_html_f4d0f9fc94544c98.png){width="3.4839in" height="0.8189in"}

Com es pot veure he pogut accedir-hi als recursos

![](img/projecte_html_1b185183e36dc19e.png){width="3.9953in" height="0.6783in"}

Comprovo des del meu servidor de SAMBA si aquest ordinador Windows
realment està dins del meu domini

![](img/projecte_html_bc00ef2a8309d69d.png){width="3.3311in" height="0.728in"}

Ara torno a provar d'entrar però sense posar abans del nom de l'usuari
el nom del domini

![](img/projecte_html_ceed5a68780543db.png){width="2.439in" height="2.7453in"}

Amb això fet ja he comprovat que aquests usuaris estan dins del meu
domini SAMBA.

# **Instal·lació i configuració client Ubuntu**<a name="debian"></a>

## **Instal·lació**<a name="install-deb"></a>

Creació i instal·lació màquina virtual

![](img/projecte_html_5f817acada5025cc.png){width="5.9756in" height="3.4354in"}

Crear xarxa per al domini de SAMBA \"192.168.1.0/24\"

![](img/projecte_html_da8a07b23ba16d01.png){width="6.2953in" height="3.75in"}

Per connectar-se a internet utilitzaré el mode pont per connectar-me a
la xarxa del meu ordinador

![](img/projecte_html_7e0f535bef9ea7c9.png){width="3.9146in" height="2.3382in"}

Creo l\'usuari

![](img/projecte_html_a0da0e31f2555850.png){width="3.2563in" height="2.1646in"}

-   *passwd:clientsamba*

Actualitzo els paquets i repositoris de la màquina virtual i instal·lo
els paquets necessaris per a la configuració del client

![](img/projecte_html_c4bfe5ada707a2a3.png){width="3.6563in" height="0.2327in"}

![](img/projecte_html_c30c95446d50b64.png){width="4.9689in" height="0.2189in"}

![](img/projecte_html_9465587bee3f4f3e.png){width="6.6728in" height="0.2217in"}

Mentre es fa la instal·lació, es mostraran diversos missatges de
Kerberos, els hauré d'omplir de la següent manera:

![](img/projecte_html_e27843f9c428afc4.png){width="5.6228in" height="1.8335in"}

![](img/projecte_html_b1d803de9f1a5085.png){width="6.2681in" height="1.139in"}

![](img/projecte_html_b92d1a5f605ed2c.png){width="6.2681in" height="1.0835in"}

Trec l'adaptador pont de la màquina virtual

![](img/projecte_html_1b33cf172f508375.png){width="5.7453in" height="3.4311in"}

## **Xarxa**<a name="config-deb"></a>

Entro dins d'eines de configuració de la xarxa

![](img/projecte_html_3eef56bea7839195.png){width="2.261in" height="2.4953in"}

Entro dins de la configuració de la interfície

![](img/projecte_html_9c1d89fceeefb77a.png){width="6.2681in" height="0.9583in"}

Poso la IP estàtica de forma manual amb la IP del servidor Samba com a
gateway i dns

![](img/projecte_html_b5d6086d2eae60d.png){width="5.0575in" height="3.4437in"}

Modifico el fitxer de configració hosts amb les següents línies:

![](img/projecte_html_a05667cb22027537.png){width="5in" height="0.2811in"}

![](img/projecte_html_34130fb46f9899ee.png){width="4.2965in" height="1.6638in"}

Comprovo si em fa ping amb totes les variants que de host

![](img/projecte_html_487d61a3ece477bd.png){width="5.161in" height="4.9244in"}

## **Configuració client Samba**

M'autentico a Kerberos

![](img/projecte_html_a52a2342c3beb61d.png){width="6.2681in" height="0.528in"}

Miro els tiquets de Kerberos obtinguts

![](img/projecte_html_3df881b4a870049e.png){width="5.9374in" height="1.3335in"}

Faig una còpia de seguretat del fitxer de configuració de samba

![](img/projecte_html_e4dec363e8b310d1.png){width="5.7291in" height="0.3752in"}

Modifico el fitxer de configuració de samba

![](img/projecte_html_eb2c4a68346dcc8b.png){width="5.8126in" height="0.2189in"}

![](img/projecte_html_cfa14c30b9172a46.png){width="3.9583in" height="4.1252in"}

Reinicio els serveis **smbd** i **nmbd**, i paro el servei samba-ad-dc

![](img/projecte_html_bb514cb487a79d66.png){width="6.2602in" height="0.3543in"}

Habilito els serveis **smbd** i **nmbd**

![](img/projecte_html_7a22a1f6a67599d9.png){width="6.2681in" height="0.639in"}

Sinconització de temps

![](img/projecte_html_cea6d6a7912895ca.png){width="6.2189in" height="0.2189in"}

![](img/projecte_html_689a109198ddde75.png){width="2.448in" height="0.4374in"}

Verifica que el cliente Ubuntu está sincronizando el tiempo con tu
servidor Samba AD:

![](img/projecte_html_1f2ab0eeeec7d678.png){width="5.7866in" height="2.922in"}

## **El SID del servidor SAMBA**

Conèixer el SID del servidor i posar el matiex per als dos, ja que pot
donar errors si no es tenen iguals:

![](img/projecte_html_8f7792f91ea5f4fc.png){width="6.2681in" height="0.9862in"}

Uneixo el client al meu domini Active Directory

![](img/projecte_html_ef0bf977c4f46abd.png){width="6.2681in" height="0.7362in"}

Des del servidor Samba AD miro si la màquina virtual client realment
s'haintegrat correctament al meu dmoni

![](img/projecte_html_1679297dcbc3fc1b.png){width="3.8335in" height="0.698in"}

Modifico el fitxer de configuració de nsswitch amb la intenció de crear
un sistema de resolució de noms

![](img/projecte_html_769aef93f7e30a2.png){width="5.7398in" height="0.198in"}

![](img/projecte_html_99f9bd004adcbdce.png){width="5.1075in" height="2.8598in"}

Reinico el servei winbind

![](img/projecte_html_4b1b22baaa68eecd.png){width="6.0417in" height="0.2083in"}

Comprovo els grups del meu domini

![](img/projecte_html_e3bb6e1241c997e7.png){width="4.5209in" height="1.5417in"}


# **Webgrafia**

**Imagenes utilitzades**

Windows 10 (ISO)

[*https://www.microsoft.com/es-es/software-download/windows10ISO*](https://www.microsoft.com/es-es/software-download/windows10ISO)

Ubuntu Desktop

[*https://ubuntu.com/desktop*](https://ubuntu.com/desktop)

Ubuntu Server

[*https://ubuntu.com/server*](https://ubuntu.com/server)

## Anexos
<i>SAMBA 4 AD [Fotografia], per SOULinux Open Source Consulting, 2019, SOULinux (https://www.soulinux.com/blog/item/7-active-directory-com-samba-4-parte-1).<a name="nota1"></a></i>
