# m14

## Repte1
#### <ins>Català</ins>
### Fitxes:
* Dockerfile

### Objectiu del repte:
Crear una imatge de Debian en la qual dins d'ell i tingui uns paquets instal·lats que no te el Debian per defecte.

En aquest cas els paquets són: **nmap**, **tree**, **vim**, **ip** i **ps**.


#### <ins>Español</ins>
### Ficheros:
* Dockerfile

### Objetivo del reto:
Crear una imagen de Debian en la que dentro de él y tenga unos paquetes instalados que no tiene Debian por defecto.

En este caso, los paquetes son: **nmap**, **tree**, **vim**, **ip** i **ps**.


#### <ins>English</ins>
### Files:
* Dockerfile

### Challenge objective:
Create a Debian image in which inside it you have some packages installed that Debian does not have by default.

In this case, the packages are: **nmap**, **tree**, **vim**, **ip** i **ps**.
***

## Repte2
#### <ins>Català</ins>
### Fitxes:
* Dockerfile
* index.html
* startup.sh

### Objectiu del repte:
Crear una imatge de Debian que generarà un servidor web al crear el container en detach.

#### <ins>Español</ins>
### Ficheros:
* Dockerfile
* index.html
* startup.sh

### Objetivo del reto:
Crear una imagen de Debian que generará un servidor web al crear el container en detach.

#### <ins>English</ins>
### Files:
* Dockerfile
* index.html
* startup.sh

### Challenge objective
Create a Debian image that will generate a web server when creating the detach container.
***

## Repte3
#### <ins>Català</ins>
### Fitxes:

### Objectiu del repte:

#### <ins>Español</ins>
### Ficheros:

### Objetivo del reto:

#### <ins>English</ins>
### Files:

### Challenge objective:

***

## Repte4
#### <ins>Català</ins>
### Fitxes:
* Dockerfile
* startup.sh
    * startup_begin.sh (Primer cop d'entrar al  container).
    * startup_after.sh (A partir del segon cop d'entrar al container).
* edt-org.ldif
* slapd.conf

### Objectiu del repte:


#### <ins>Español</ins>
### Ficheros:

### Objetivo del reto:

#### <ins>English</ins>
### Files:

### Challenge objective:
